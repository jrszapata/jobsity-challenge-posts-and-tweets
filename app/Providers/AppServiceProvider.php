<?php

namespace App\Providers;

use DG\Twitter\Twitter;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        $twitter = new Twitter(
//            env('TWITTER_API_KEY'),
//            env('TWITTER_API_SECRET'),
//            env('TWITTER_TOKEN_KEY'),
//            env('TWITTER_TOKEN_SECRET')
//        );
//        app()->singleton('twitter', $twitter);
    }
}
