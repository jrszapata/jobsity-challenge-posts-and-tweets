<?php

namespace App\Http\Controllers;

use App\Tweet;
use Illuminate\Http\Request;
use App\User;
use App\Post;
use DG\Twitter\Twitter;

class ProfileController extends Controller
{
    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user)
    {
        $posts = Post::where('author_id', $user->id)->orderByDesc('created_at')->paginate(3);

        $tweets = $this->getUserTweets($user);

        return view('profile.show', [
            'author' => $user,
            'posts' => $posts,
            'tweets' => $tweets,
        ]);
    }

    /**
     * Get Latest Tweets
     * @param User $user
     * @return array|\stdClass|\stdClass[]|null
     */
    public function getUserTweets(User $user)
    {
        if ($user->hasTwitter()) {
            $latestTweets = $this->fetchTweets($user->twitter_username, 5);
            $ids = array_column($latestTweets, 'id');
            $hidden = Tweet::whereIn('tweet_id', $ids)->get()->pluck('tweet_id');
            return $user->signedIn()
                ? $this->markAsHidden($latestTweets, $hidden)
                : $this->forgetHidden($latestTweets, $hidden);
        }
        return null;
    }

    /**
     * Filter all hidden tweets
     * @param $tweets
     * @param $hidden
     * @return array
     */
    public function markAsHidden($tweets, $hidden)
    {
        $hiddenMap = (object) $hidden->flip()->toArray();
        foreach($tweets as $tweet) {
            $tweet->hidden = property_exists($hiddenMap, $tweet->id);
        }
        return $tweets;
    }

    /**
     * Mark tweets as hidden according to a list of provide ids
     * @param $tweets
     * @param $hidden
     * @return array
     */
    public function forgetHidden($tweets, Array $hidden)
    {
        $hiddenMap = (object) $hidden->flip()->toArray();
        return array_filter($tweets, function($tweet) use($hiddenMap) {
            return !property_exists($hiddenMap, $tweet->id);
        });
    }

    /**
     * Fetch latest tweets by twitter nickname
     * @param $nickname
     * @param $count
     * @return \stdClass|\stdClass[]
     * @throws \DG\Twitter\Exception
     */
    public function fetchTweets($nickname, $count) {

        $twitter = new Twitter(
            env('TWITTER_API_KEY'),
            env('TWITTER_API_SECRET'),
            env('TWITTER_TOKEN_KEY'),
            env('TWITTER_TOKEN_SECRET')
        );
        return $twitter->request('statuses/user_timeline', 'GET', ['count' => $count, 'screen_name' => $nickname]);
    }

}
