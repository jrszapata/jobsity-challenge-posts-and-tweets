<?php

namespace App\Http\Controllers;

use App\Tweet;

class TweetController extends Controller
{
    /**
     * Make the tweet visible
     * @param $tweetId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function showTweet($tweetId)
    {
        $tweet = auth()
            ->user()
            ->tweets()
            ->where(['tweet_id' => $tweetId,])
            ->first();

        $this->authorize('update', $tweet);

        if ($tweet) {
            $tweet->delete();
        }
        return response('', 200);
    }

    /**
     * Make the tweet invisible to other users
     * @param $tweetId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function hideTweet($tweetId)
    {
        if (!auth()->user()->tweets()->where(['tweet_id' => $tweetId])->exists()) {
            Tweet::create([
                'user_id' => auth()->id(),
                'tweet_id' => $tweetId
            ]);
        }
        return response('', 200);
    }
}
