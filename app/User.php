<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;
use Laravel\Socialite\Contracts\User as SocialiteUser;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'twitter_username', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot() {
      parent::boot();
        static::creating(function($user){
            $user->api_token = Str::random(60);
        });
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'author_id');
    }

    public function isOwnerOf(Post $post)
    {
        if ($post && $post->author_id == $this->id) {
            return true;
        }
        return false;
    }

    public function tweets()
    {
        return $this->hasMany(Tweet::class, 'user_id');
    }

    /**
     * @param SocialiteUser $user
     * @return mixed
     */
    public static function createFromTwitter(SocialiteUser $user)
    {
        return static::create([
            'email' => $user->getEmail(),
            'twitter_username' => $user->getNickname(),
            'name' => $user->getName(),
        ]);
    }

    protected function setSocialiteAttribute(bool $value)
    {
        $this->attributes['socialite'] = $value ? 1 : 0;
    }

    public function hasTwitter()
    {
        return !is_null($this->twitter_username);
    }


    public function signedIn()
    {
        return auth()->id() == $this->id;
    }
}
