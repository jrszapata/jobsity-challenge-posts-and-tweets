<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'content', 'author_id'];


    public function setContentAttribute($value)
    {
        $this->attributes['content'] = html_entity_decode($value);
    }

    //
    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function getBody()
    {
        return html_entity_decode($this->content);
    }
}
