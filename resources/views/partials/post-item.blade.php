@html($html)
<section class="card mb-3">
    <div class="card-body">
        <h2 class="mb-3">{{ $post->title }}</h2>
        <section>{!! $post->content !!}</section>
        <p class="text-muted mb-0">
            <span>By <a href="/{{$post->author->username}}">{{ $post->author->name }}</a></span>
            <span>on {{ $post->created_at->format('M d, Y \a\t h:m a') }}</span>
        </p>
        @if(Auth::check() && auth()->user()->isOwnerOf($post))
            <div class="actions float-right">
                <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">
                    <i class="fa fa-pencil"></i> Edit
                </a>
            </div>
        @endif

    </div>
</section>
