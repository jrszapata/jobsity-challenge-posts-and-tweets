@extends('layouts.app')

@section('content')
    <section class="container" id="profile">

        <header class="mt-5 mb-4 d-flex align-items-center">
            <h1 class="mb-0 pb-0">{{ $author->name }}</h1>
            <span class="badge badge-info ml-2">{{ $author->username }}</span>
        </header>

        <div class="row">
            <div class="col-md-8">
                <section class="d-flex justify-content-between">
                    <h2>Posts</h2>
                    <div>{{ $posts->links() }}</div>
                </section>
                @foreach($posts as $post)
                    @include('partials.post-item', ['post' => $post])
                @endforeach
            </div>

            <div class="col-md-4">
                <section class="mb-3">
                    <h2 class="mb-0">Tweets</h2>
                    @if($author->hasTwitter())
                        <span class="text-muted">by
                            @if($author->signedIn())
                            Me
                            @else
                            {{ $author->name }}
                            <a href="https://twitter.com/{{$author->twitter_username}}" target="_blank" class="badge badge-info">
                                {{ '@' . $author->twitter_username }}
                            </a>
                            @endif
                        </span>
                    @elseif($author->signedIn())
                        <div>
                            <span class="text-muted">You don't have a twitter linked to your account.</span>
                            <a href="/login/twitter" class="btn btn-primary">
                                <i class="fa fa-twitter"></i>
                                Link Twitter
                            </a>
                        </div>
                    @else
                        <span class="text-muted">{{ $author->name }} has not a twitter linked to his account</span>
                    @endif
                </section>
                @if($tweets)
                @foreach($tweets as $tweet)
                    <section class="card mb-3">
                        <div class="card-body">
                            <p class="mb-0">{{ $tweet->text }}</p>
                            @if($author->signedIn())
                            <tweet-hider class="mt-2 mb-0" tweet-id="{{$tweet->id}}" hidden="{{ $tweet->hidden }}"></tweet-hider>
                            @endif
                        </div>
                    </section>
                @endforeach
                @endif
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    @if($author->signedIn())
        <script>window.API_TOKEN="{{ auth()->user()->api_token }}";</script>
    @endif
@endsection
