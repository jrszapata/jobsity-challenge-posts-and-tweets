@extends('layouts.app')

@section('content')

    <section class="container">
        <h1 class="mt-5 mb-3">New Post</h1>

        @include('posts.form', ['post' => $post ?? new App\Post()])
    </section>
@endsection
