@extends('layouts.app')

@section('content')
    <section class="container">
        <h1 class="mt-5 mb-3">Posts</h1>
        <div class="d-flex justify-content-between">
            @if(Auth::check())
                <section>
                    <a href="/posts/create" class="btn btn-primary">Create Post</a>
                </section>
                <section>
                    {{ $posts->links() }}
                </section>
            @endif
        </div>
        @foreach($posts as $post)
            @include('partials.post-item', ['post' => $post])
        @endforeach
    </section>
@endsection
