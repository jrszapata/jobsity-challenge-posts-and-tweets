<form action="/posts/{{$post->id}}" method="POST">
    @if($post->id)
        @method('PATCH')
    @endif
    @csrf()

    <div class="row">
        <div class="form-group col-md-6">
            <label for="title">Title</label>
            <input type="text" name="title" id="title" value="{{ old('title', $post->title) }}" class="form-control" />
        </div>
    </div>
    @error('title')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="row">
        <div class="form-group col-md-12">
            <label for="content">Content</label>
            <post-editor content="{{ old('content', $post->content) }}"></post-editor>
        </div>
    </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="actions">
        <a href="/" class="btn btn-secondary">Cancel</a>
        <button type="submit" class="btn btn-primary">
            @if($post->id) Update @else Create @endif Post
        </button>
    </div>
</form>
