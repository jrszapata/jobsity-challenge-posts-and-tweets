@extends('layouts.app')

@section('content')

    <section class="container">
        <h1 class="mt-5 mb-3">Edit Post</h1>

        @include('posts.form')
    </section>
@endsection
