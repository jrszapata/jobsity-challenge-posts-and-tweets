require('./bootstrap');

window.Vue = require('vue');

Vue.component('post-editor', require('./components/PostEditorComponent').default);
Vue.component('tweet-hider', require('./components/TweetHiderComponent').default);

const app = new Vue({
    el: '#app',
});
