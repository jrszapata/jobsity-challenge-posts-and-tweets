<?php


Auth::routes();


Route::middleware('auth')->group(function() {
    Route::get('posts/create', 'PostController@create');
    Route::get('posts/{post}/edit', 'PostController@edit');
    Route::post('posts', 'PostController@store');
    Route::patch('posts/{post}', 'PostController@update');

});

Route::middleware('auth:api')->group(function() {
    Route::post('/tweets/{tweeId}/show', 'TweetController@showTweet');
    Route::post('/tweets/{tweeId}/hide', 'TweetController@hideTweet');
});

Route::get('login/twitter', 'Auth\LoginController@redirectToProvider');
Route::get('login/twitter/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/{user}', 'ProfileController@show');
Route::get('/', 'PostController@index')->name('home');
Route::redirect('/home', '/');

