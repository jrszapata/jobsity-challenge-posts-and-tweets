
## Jobsity Challenge by Stanley Zapata

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

This is the attempt to complete the challenge of Jobsity for the PHP language.

I used PHP, Laravel, SQLite (although any popular Db provider can be used easily) and Vue.

### Setting up

Find a PC with Ubuntu Linux 18.04 or newer and run:

```sh
sudo apt-get update
```


#### PHP

```sh
sudo add-apt-repository ppa:ondrej/php
sudo apt-get install software-properties-common
sudo apt-get update
sudo apt-get -y install unzip zip php7.3 php7.3-mysql php7.3-sqlite php7.3-fpm php7.3-mbstring php7.3-curl php7.3-dom
```

Install Composer package manager

```sh
sudo apt-get -y install composer
```
Pick a app directory. `/var/www/html` is a good option. Create it if it doesn't exist and cd into it.

```sh
sudo mkdir /var/www/html -p
cd /var/www/html
```

Clone this repository inside the app directory
```sh
sudo git clone https://bitbucket.org/jrszapata/jobsity-challenge-posts-and-tweets.git ./
```

Create vendor directory. 
```sh
sudo mkdir vendor
```

Set some permissions

```sh
sudo chown -R www-data:www-data ./
sudo chmod -R 775 storage bootstrap/cache vendor
```

To install dependencies add the current user to the `www-data`  
```sh
sudo usermod -a -G www-data $USER
su $USER # Or reboot the OS
```

Install project dependencies with composer
```sh
composer install
```

If using default .env-xample. Rename to .env
```sh
mv .env.example .env
```
Create a SQLite database file
```sh
touch storage/database.sqlite
```


Optional. If you want to test without Nginx. You can serve the app using php build in server

```sh
php artisan serve 
```

#### Nginx

Install Nginx and create a config file

```sh
apt-get install -y nginx
touch /etc/nginx/sites_enabled/challenge
```

Copy the next configuration onto that file:

```nginx
server {
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;
    root /var/www/html/public;
    index index.php index.html index.htm;
    server_name localhost;
    charset   utf-8;
    gzip on;
    gzip_vary on;
    gzip_disable "msie6";
    gzip_comp_level 6;
    gzip_min_length 1100;
    gzip_buffers 16 8k;
    gzip_proxied any;
    gzip_types
        text/plain
        text/css
        text/js
        text/xml
        text/javascript
        application/javascript
        application/x-javascript
        application/json
        application/xml
        application/xml+rss;
    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }
    location ~ \.php\$ {
        try_files \$uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)\$;
        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
    }
    location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc|svg|woff|woff2|ttf)\$ {
      expires 1M;
      access_log off;
      add_header Cache-Control "public";
    }
    location ~* \.(?:css|js)\$ {
      expires 7d;
      access_log off;
      add_header Cache-Control "public";
    }
    location ~ /\.ht {
        deny  all;
    }
}
```
Feel free to modify the `server_name` directive to the one you prefered.


Execute Nginx

```bash
/etc/init.d/nginx restart
```

Open the browser and test `http://localhost`
